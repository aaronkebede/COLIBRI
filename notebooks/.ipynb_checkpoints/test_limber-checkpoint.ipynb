{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "informed-notion",
   "metadata": {},
   "source": [
    "# Test of `limber` module\n",
    "\n",
    "Compute the angular power spectrum or correlation function in Limber's and flat-sky approximations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "satisfied-offense",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import colibri.limber as LL\n",
    "import colibri.cosmology as cc\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.rc('text',usetex=True)\n",
    "plt.rc('font',size=25,family='serif')\n",
    "\n",
    "# Colors\n",
    "colors = ['r', 'b','g','goldenrod','m', 'k', 'springgreen', 'darkorange', 'pink', 'darkcyan', 'salmon']\n",
    "# Linewidth\n",
    "LW = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "precious-genre",
   "metadata": {},
   "source": [
    "Set number of bins and whether to compute power spectrum or correlation function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "blond-technology",
   "metadata": {},
   "outputs": [],
   "source": [
    "nbins         = 5    # Number of bins to use (choose among 3,4,5)\n",
    "fourier_space = True # If True compute the spectra in Fourier space; if False, compute the correlation functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "assured-booth",
   "metadata": {},
   "source": [
    "### Initialize cosmology and limber instance\n",
    "\n",
    "The `limber_3x2pt` instance takes as arguments\n",
    " * a cosmology instance:\n",
    " * a 2-uple or a list of length 2, whose values are the lower and upper limit of integration in redshift"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "separated-package",
   "metadata": {},
   "outputs": [],
   "source": [
    "C = cc.cosmo()\n",
    "S = LL.limber_3x2pt(cosmology = C, z_limits = [0.01, 5.])\n",
    "print(\"> Limber instance loaded\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "competitive-support",
   "metadata": {},
   "source": [
    "### Load power spectra\n",
    "\n",
    "The routine `load_power_spectra` interpolates the power spectra at the scales and redshifts asked.\n",
    "One needs to pre-compute (or measure from simulations) the matter power spectra and feed the array of shape (nz,nk) to the routine, that creates an interpolated object `self.power_spectra_interpolator`.\n",
    "Also galaxy bias can be fed to the routine: a quantity names `self.galaxy_bias_interpolator` will be created as well (if not given, such quantity will always return 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interracial-protest",
   "metadata": {},
   "outputs": [],
   "source": [
    "kk = np.geomspace(1e-4, 1e2, 301) # scales in h/Mpc\n",
    "zz = np.linspace(0., 5., 51)      # redshifts\n",
    "ZZ,KK = np.meshgrid(zz,kk,indexing='ij')\n",
    "\n",
    "# Non-linear power spectra (array of shape (nz,nk))\n",
    "_, pkz = C.camb_Pk(z = zz, k = kk, nonlinear = True, halofit = 'mead2020')\n",
    "# Galaxy bias (array of shape (nz,nk))\n",
    "bkz = (1.+ZZ)**0.5\n",
    "\n",
    "# Load spectra\n",
    "S.load_power_spectra(z = zz, k = kk, power_spectra = pkz, galaxy_bias = bkz)\n",
    "print(\"> Power spectra loaded\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "desperate-agency",
   "metadata": {},
   "source": [
    "### Load window functions\n",
    "\n",
    "The function `load_window_function` computes the window functions for all the bins that are required, given the galaxy distribution.\n",
    "It takes as arguments two arrays:\n",
    " * `z`: a 1-D array containing the redshifts at which the galaxy distribution is evaluated\n",
    " * `nz`: a 2-D array containing the galaxy distribution for each bin. Its first dimension is the number of bins, the second dimension must be the same as `z`\n",
    " \n",
    "Nothing is returned, but a dictionary named `self.window_function` containing the keys `g` for cosmic shear and `I` for intrinsic alignment is created. Each key is a list of length `n_bin` of interpolated objects that contain the corresponding window function as a function of redshift."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "animated-familiar",
   "metadata": {},
   "outputs": [],
   "source": [
    "if nbins == 3:\n",
    "    bin_edges = [0.01, 0.72, 1.11, 5.00]\n",
    "elif nbins == 4:\n",
    "    bin_edges = [0.01, 0.62, 0.90, 1.23, 5.00]\n",
    "elif nbins == 5:\n",
    "    bin_edges = [0.01, 0.56, 0.79, 1.02, 1.32, 5.00]\n",
    "else:\n",
    "    raise ValueError(\"Choose among 3,4 or 5 bins (or implement your own set of galaxy distributions).\")\n",
    "# Load galaxy distributions\n",
    "z_tmp     = np.linspace(S.z_min, S.z_max, 201)\n",
    "nz_tmp    = [S.euclid_distribution_with_photo_error(z=z_tmp,zmin = bin_edges[i], zmax = bin_edges[i+1]) for i in range(nbins)]\n",
    "# Window functions\n",
    "S.load_window_functions(z = z_tmp, nz = nz_tmp)\n",
    "print(\"> Window functions loaded\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "structural-potential",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot galaxy distributions and window functions\n",
    "hf, axarr = plt.subplots(2, 1, sharex=True, figsize=(15,10))\n",
    "plt.subplots_adjust(hspace = 0.)\n",
    "zz = np.linspace(0.1, 5., 1000)\n",
    "for i in range(nbins):\n",
    "    axarr[0].plot(zz, S.window_function['g'][i](zz)*1e5, colors[i],            lw = LW, label = 'Bin %i' %(i+1))\n",
    "    axarr[1].plot(zz, S.window_function['I'][i](zz)*1e3, colors[i], ls = '--', lw = LW)\n",
    "axarr[1].set_xlabel('$z$')\n",
    "axarr[0].set_xlim(zz.min(), zz.max())\n",
    "axarr[0].set_ylabel('$10^5 \\\\times W_\\gamma     (z) \\ [h/\\mathrm{Mpc}]$', fontsize = 20)\n",
    "axarr[1].set_ylabel('$10^3 \\\\times W_\\mathrm{IA}(z) \\ [h/\\mathrm{Mpc}]$', fontsize = 20)\n",
    "axarr[0].legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "combined-essex",
   "metadata": {},
   "source": [
    "### Compute power spectra or correlation function\n",
    "\n",
    "The routines `limber_angular_power_spectra` and `limber_angular_correlation_functions` take as argument:\n",
    " * an array of multipoles or angles in arcmin\n",
    " * `do_WL`, `do_IA` and `do_GC`, three boolean quantities determining whether to compute cosmic shear, intrinsic alignment and galaxy clustering\n",
    " * `A_IA`, `beta_IA`, `eta_IA`: three floats for the non-linear alignment model, used for intrinsic alignment\n",
    " * `lum_IA`: the ratio between the mean luminosity of the sample and the typical luminosity at a given redshift. Can be either a float or a function whose ONLY argument is redshift.\n",
    " \n",
    "The quantity returned is a dictionary containing different keys:\n",
    " * `'gg'`: cosmic shear signal\n",
    " * `'gI'`: cross term of cosmic shear and intrinsic alignment\n",
    " * `'II'`: pure intrinsic alignment signal\n",
    " * `'LL'`: lensing power spectrum (i.e. 'gg'+'gI'+'II')\n",
    " * `'GL'`: galaxy-galaxy lensing angular power spectrum\n",
    " * `'GG'`: angular galaxy clustering\n",
    " \n",
    "If the correlation function is asked, the keys `'gg'`, `'gI'`, `'II'`, `'LL'` appear with both `+` and `-`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cubic-analyst",
   "metadata": {},
   "outputs": [],
   "source": [
    "if fourier_space: \n",
    "    ll     = np.geomspace(2., 1e4, 51)\n",
    "    Cl     = S.limber_angular_power_spectra(l            = ll,\n",
    "                                            do_WL        = True,\n",
    "                                            do_IA        = True,\n",
    "                                            do_GC        = True,\n",
    "                                            A_IA = -1.3, beta_IA = 0., eta_IA = 0., lum_IA = 1.)\n",
    "    print(\"> Spectra loaded\")\n",
    "else:\n",
    "    theta = np.geomspace(10., 800., 51)      # in arcmin\n",
    "    xi    = S.limber_angular_correlation_functions(theta        = theta,\n",
    "                                                   do_WL        = True,\n",
    "                                                   do_IA        = True,\n",
    "                                                   do_GC        = True,\n",
    "                                                   A_IA = -1.3, beta_IA = 0., eta_IA = 0., lum_IA = 1.)\n",
    "    print(\"> Correlation functions loaded\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "urban-punishment",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Colors\n",
    "colors = ['r', 'b','g','goldenrod','m', 'k', 'springgreen', 'darkorange', 'pink', 'darkcyan', 'salmon']\n",
    "# Linewidth\n",
    "LW = 2\n",
    "if fourier_space:\n",
    "    # Plot shear spectra\n",
    "    hf, axarr = plt.subplots(nbins, nbins, sharex = True, sharey = True, figsize=(15,10))\n",
    "    # Multiplication constant for plotting\n",
    "    c = ll*(ll+1.)/(2.*np.pi)\n",
    "    for j in range(1, nbins):\n",
    "        for i in range(j):\n",
    "            axarr[i,j].axis('off')\n",
    "        plt.setp([a.get_xticklabels() for a in axarr[i, :]], visible=False)\n",
    "        plt.setp([a.get_yticklabels() for a in axarr[:, j]], visible=False)\n",
    "        plt.subplots_adjust(wspace=0, hspace=0)\n",
    "\n",
    "    for i in range(nbins):\n",
    "        for j in range(i, nbins):\n",
    "            # Plotting Cls and systematics\n",
    "            axarr[j,i].loglog(ll, c*Cl['gg'][i,j],         'blue'     , ls='-' , lw=LW, label='$C_\\mathrm{\\gamma\\gamma}^{(ij)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, np.abs(c*Cl['gI'][i,j]), 'magenta'  , ls='-' , lw=LW, label='$C_\\mathrm{\\gamma I}^{(ij)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, c*Cl['II'][i,j],         'red'      , ls='-' , lw=LW, label='$C_\\mathrm{II}^{(ij)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, c*Cl['LL'][i,j],         'black'    , ls='-' , lw=LW, label='$C_\\mathrm{LL}^{(ij)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, c*Cl['GL'][i,j],         'green'    , ls='-' , lw=LW, label='$C_\\mathrm{GL}^{(ij)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, c*Cl['GL'][j,i],         'limegreen', ls='--', lw=LW, label='$C_\\mathrm{GL}^{(ji)}(\\ell)$')\n",
    "            axarr[j,i].loglog(ll, c*Cl['GG'][i,j],         'goldenrod', ls='-' , lw=LW, label='$C_\\mathrm{GG}^{(ij)}(\\ell)$')\n",
    "            # Coloured box\n",
    "            if i != j: color = 'grey'\n",
    "            else:      color = colors[i]\n",
    "            axarr[j,i].text(0.15, 0.85, '$%i \\\\times %i$' %(i+1,j+1),\n",
    "                            transform=axarr[j,i].transAxes,\n",
    "                            style='italic',\n",
    "                            fontsize = 15,\n",
    "                            horizontalalignment='center',\n",
    "                            bbox={'facecolor': color, 'alpha':0.5, 'pad':5})\n",
    "            axarr[j,i].set_xlim(ll.min(), ll.max())\n",
    "            axarr[j,i].set_ylim(5e-10, 1e0)\n",
    "            axarr[j,i].set_yticks([1e-8,1e-5,1e-2])\n",
    "    plt.legend(bbox_to_anchor=(0.9, nbins))\n",
    "\n",
    "    # Single label\n",
    "    hf.add_subplot(111, frameon=False)\n",
    "    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)\n",
    "    plt.xlabel(\"$\\ell$\")\n",
    "    plt.ylabel(\"$\\ell(\\ell+1) \\ C_\\ell \\ / \\ (2\\pi)$\", labelpad = 35)\n",
    "\n",
    "else:\n",
    "    # Plot correlation functions\n",
    "    hf, axarr = plt.subplots(nbins, nbins, sharex = True, sharey = True, figsize=(15,10))\n",
    "    for j in range(1, nbins):\n",
    "        for i in range(j):\n",
    "            axarr[i,j].axis('off')\n",
    "        plt.setp([a.get_xticklabels() for a in axarr[i, :]], visible=False)\n",
    "        plt.setp([a.get_yticklabels() for a in axarr[:, j]], visible=False)\n",
    "        plt.subplots_adjust(wspace=0, hspace=0)\n",
    "\n",
    "    for i in range(nbins):\n",
    "        for j in range(i, nbins):\n",
    "            # Plotting Cls and systematics\n",
    "            axarr[j,i].loglog(theta, xi['gg+'][i,j],         'blue'     , ls='-' , lw=LW, label='$\\\\xi_\\mathrm{\\gamma\\gamma}^{+,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['gg-'][i,j],         'blue'     , ls='--', lw=LW, label='$\\\\xi_\\mathrm{\\gamma\\gamma}^{-,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, np.abs(xi['gI+'][i,j]), 'magenta'  , ls='-' , lw=LW, label='$\\\\xi_\\mathrm{\\gamma I}^{+,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, np.abs(xi['gI-'][i,j]), 'magenta'  , ls='--', lw=LW, label='$\\\\xi_\\mathrm{\\gamma I}^{-,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['II+'][i,j],         'red'      , ls='-' , lw=LW, label='$\\\\xi_\\mathrm{II}^{+,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['II-'][i,j],         'red'      , ls='--', lw=LW, label='$\\\\xi_\\mathrm{II}^{-,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['LL+'][i,j],         'black'    , ls='-' , lw=LW, label='$\\\\xi_\\mathrm{LL}^{+,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['LL-'][i,j],         'black'    , ls='--', lw=LW, label='$\\\\xi_\\mathrm{LL}^{-,(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['GL'] [i,j],         'green'    , ls='-' , lw=LW, label='$\\\\xi_\\mathrm{GL}^{(ij)}(\\\\theta)$')\n",
    "            axarr[j,i].loglog(theta, xi['GG'] [i,j],         'goldenrod', ls='-' , lw=LW, label='$\\\\xi_\\mathrm{GG}^{(ij)}(\\\\theta)$')\n",
    "\n",
    "\n",
    "            # Coloured box\n",
    "            if i != j: color = 'grey'\n",
    "            else:      color = colors[i]\n",
    "            axarr[j,i].text(0.15, 0.15, '$%i \\\\times %i$' %(i+1,j+1),\n",
    "                            transform=axarr[j,i].transAxes,\n",
    "                            style='italic',\n",
    "                            fontsize = 15,\n",
    "                            horizontalalignment='center',\n",
    "                            bbox={'facecolor': color, 'alpha':0.5, 'pad':5})\n",
    "            axarr[j,i].set_xlim(theta.min(), theta.max())\n",
    "            axarr[j,i].set_ylim(1e-9, 1e-4)\n",
    "    plt.legend(bbox_to_anchor=(0.9, nbins), fontsize = 20)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "mathematical-cigarette",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
