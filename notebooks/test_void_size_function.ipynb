{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "informed-notion",
   "metadata": {},
   "source": [
    "# Test of void size function\n",
    "\n",
    "This test file shows how to use the void size function routines in the `cosmo` class.\n",
    "For more information on the quantities involved here, check out https://arxiv.org/abs/1206.3506"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dietary-isaac",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import colibri.cosmology as cc\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.interpolate as si\n",
    "\n",
    "plt.rc('text', usetex=True)\n",
    "plt.rc('font', family='serif', size = 14)\n",
    "plt.rcParams['lines.linewidth'] = 1.5\n",
    "\n",
    "ls     = ['-','--',':','-.']\n",
    "colors = ['b','r','g','goldenrod','m','k']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fallen-demonstration",
   "metadata": {},
   "source": [
    "Fixed parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "stable-buddy",
   "metadata": {},
   "outputs": [],
   "source": [
    "zz      = [0.,1.,2.]                  # Redshifts\n",
    "nz      = len(np.atleast_1d(zz))      # Number of redshifts\n",
    "logM    = np.linspace(5., 17., 121)   # Masses in log-space\n",
    "R_Eul   = np.linspace(1.,35.,69)      # Eulerian radii at which to compute voids\n",
    "delta_v = -1.76                       # Linear extrapolation of underdensity for voids at \"collapse\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "educational-pension",
   "metadata": {},
   "source": [
    "### Cosmology instance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "behind-camel",
   "metadata": {},
   "outputs": [],
   "source": [
    "C = cc.cosmo()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "independent-cathedral",
   "metadata": {},
   "source": [
    "### Load linear power spectra"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "buried-worker",
   "metadata": {},
   "outputs": [],
   "source": [
    "k, pk = C.camb_Pk(z = zz)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "interstate-biotechnology",
   "metadata": {},
   "source": [
    "### Mass variance multipoles\n",
    "\n",
    "We smooth with a second Gaussian filter with size 5.5 Mpc/h (useful for voids)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "related-dollar",
   "metadata": {},
   "outputs": [],
   "source": [
    "s0 = C.mass_variance_multipoles(logM=logM,k=k,pk=pk,j=0,smooth=True,window='th',R_sm=5.5)\n",
    "s1 = C.mass_variance_multipoles(logM=logM,k=k,pk=pk,j=1,smooth=True,window='th',R_sm=5.5)\n",
    "s2 = C.mass_variance_multipoles(logM=logM,k=k,pk=pk,j=2,smooth=True,window='th',R_sm=5.5)\n",
    "\n",
    "# Compute sigma8 for secturity\n",
    "s8 = C.compute_sigma_8(k=k,pk=pk)[0]\n",
    "M8 = C.mass_in_radius(8.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decent-pontiac",
   "metadata": {},
   "source": [
    "### Useful quantities related to void size function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "governing-treat",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Transform Eulerian to Lagrangian coordinates\n",
    "R_Lag     = np.outer(C.lagrange_to_euler(z = zz),R_Eul)\n",
    "# gamma_p and R* parameters\n",
    "gamma_p   = s1/np.sqrt(s0*s2)\n",
    "R_star    = np.sqrt(3.*s1/s2)\n",
    "# Peak (i.e. Trough) height in voids\n",
    "dv        = np.abs(delta_v)\n",
    "nu        = dv/s0**.5\n",
    "# Excursion Set Troughs functions\n",
    "G1        = np.array([C.G_n_BBKS(1, gamma_p[iz], nu[iz]) for iz in range(nz)])\n",
    "# Press-Schechter formalism for voids\n",
    "R_of_M    = C.radius_of_mass(10.**logM) # Radii corresponding to masses\n",
    "f_nu      = C.volume_of_radius(R_of_M, 'th')/(2.*np.pi*R_star**2.)**(3./2.)*C.PressSchechter_mass_function(s0**.5, delta_th = dv)/(2.*nu)*G1/(gamma_p*nu)\n",
    "# High-mass approximation for Press-Schechter formalism for voids\n",
    "f_high_nu = np.exp(-nu**2./2.)/np.sqrt(2.*np.pi)*C.volume_of_radius(R_of_M, 'th')/(2.*np.pi*R_star**2.)**1.5*(nu**3.-3*nu)*gamma_p**3.\n",
    "# Mass at which peak height = 1\n",
    "M_of_nu1 = 10.**np.array([si.interp1d(nu[iz], logM, 'cubic')(1.) for iz in range(nz)])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "convertible-mexico",
   "metadata": {},
   "source": [
    "### Compute actual void size function\n",
    "\n",
    "If interested in the void size function only, one can just run this line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "amazing-light",
   "metadata": {},
   "outputs": [],
   "source": [
    "# a,p are Sheth-Tormen parameters, delta_v is the linear underdensity for \"collapse\" of voids\n",
    "RL,VSF = C.void_size_function(R=R_Eul,z=zz,k=k,pk=pk,delta_v=-1.76,a=1.,p=0.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "meaning-korea",
   "metadata": {},
   "source": [
    "### Plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "actual-pricing",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize = (12,12))\n",
    "plt.subplots_adjust(hspace = 0.3, wspace = 0.2, right = 0.95, bottom = 0.09, top = 0.95)\n",
    "\n",
    "ax1 = plt.subplot2grid((3,2), (0,0), colspan = 1, rowspan = 1)\n",
    "ax2 = plt.subplot2grid((3,2), (0,1), colspan = 1, rowspan = 1)\n",
    "ax3 = plt.subplot2grid((3,2), (1,0), colspan = 1, rowspan = 1)\n",
    "ax4 = plt.subplot2grid((3,2), (1,1), colspan = 1, rowspan = 1)\n",
    "ax5 = plt.subplot2grid((3,2), (2,0), colspan = 1, rowspan = 1)\n",
    "ax6 = plt.subplot2grid((3,2), (2,1), colspan = 1, rowspan = 1)\n",
    "\n",
    "# Mass variances and multipoles\n",
    "ax1.loglog(10.**logM, s0[0], 'b'        ,ls=ls[0],label = '$s_0$')\n",
    "ax1.loglog(10.**logM, s1[0], 'g'        ,ls=ls[0],label = '$s_1$')\n",
    "ax1.loglog(10.**logM, s2[0], 'goldenrod',ls=ls[0],label = '$s_2$')\n",
    "ax1.loglog(M8, s8**2., 'rP', label = '$\\\\sigma_8^2$', ms = 10)\n",
    "ax1.text(0.85, 0.85, '$z=%i$'%zz[0],\n",
    "         transform=ax1.transAxes,\n",
    "         style='italic',\n",
    "         fontsize = 15,\n",
    "         horizontalalignment='center',\n",
    "         bbox={'facecolor': 'white', 'alpha':0.5, 'pad':0.5, 'boxstyle':'round'})\n",
    "ax1.set_xlabel('$M \\ [M_\\odot/h]$')\n",
    "ax1.set_xlim(3e9,3e16)\n",
    "ax1.set_ylim(1e-8,1e5)\n",
    "ax1.legend(fontsize = 12,ncol=2,loc='lower left')\n",
    "\n",
    "# Useful quantities for voids\n",
    "ax2.loglog(10.**logM, nu[0],           'b'        ,ls=ls[0],label = '$\\\\nu$')\n",
    "ax2.loglog(10.**logM, R_star[0],       'r'        ,ls=ls[0],label = '$R_* \\ [\\mathrm{Mpc}/h]$')\n",
    "ax2.loglog(10.**logM, gamma_p[0],      'g'        ,ls=ls[0],label = '$\\gamma_p$')\n",
    "ax2.loglog(10.**logM, gamma_p[0]*nu[0],'goldenrod',ls=ls[0],label = '$\\gamma_p\\\\nu$')\n",
    "ax2.text(0.85, 0.85, '$z=%i$'%zz[0],\n",
    "         transform=ax2.transAxes,\n",
    "         style='italic',\n",
    "         fontsize = 15,\n",
    "         horizontalalignment='center',\n",
    "         bbox={'facecolor': 'white', 'alpha':0.5, 'pad':0.5, 'boxstyle':'round'})\n",
    "ax2.set_xlabel('$M \\ [M_\\odot/h]$')\n",
    "ax2.set_xlim(3e9,3e16)\n",
    "ax2.set_ylim(1e-1,1e2)\n",
    "ax2.legend(fontsize = 12)\n",
    "\n",
    "# Lagrangian radius\n",
    "for iz in range(nz):\n",
    "    ax3.plot(R_Eul, R_Lag[iz],c=colors[iz],label='$z=%i$'%(zz[iz]))\n",
    "ax3.set_xlabel('$R_\\mathrm{Eul} \\ [\\mathrm{Mpc}/h]$')\n",
    "ax3.set_ylabel('$R_\\mathrm{Lag} \\ [\\mathrm{Mpc}/h]$')\n",
    "ax3.set_xlim(R_Eul.min(), R_Eul.max())\n",
    "ax3.legend()\n",
    "\n",
    "# BBKS function G1\n",
    "for iz in range(nz):\n",
    "    ax4.loglog(10.**logM, G1[iz],c=colors[iz])\n",
    "    ax4.loglog(10.**logM, (gamma_p*nu*((nu**3.-3*nu)*gamma_p**3.))[iz],c=colors[iz],ls='--')\n",
    "    ax4.axvline(M_of_nu1[iz], c=colors[iz],ls=':',lw=1.)\n",
    "ax4.loglog(np.nan,np.nan, 'k:', label = 'approximation for $\\\\nu \\gg 1$')\n",
    "ax4.axvline(np.nan, c='k',ls=':',lw=0.5,label='$M(\\\\nu=1)$')\n",
    "ax4.set_xlabel('$M \\ [M_\\odot/h]$')\n",
    "ax4.set_ylabel('$G_1(\\gamma_p, \\gamma_p\\\\nu)$')\n",
    "ax4.set_xlim(3e9,3e16)\n",
    "ax4.set_ylim(1e-1,1e5)\n",
    "ax4.legend()\n",
    "\n",
    "\n",
    "# Excursion set of Troughs\n",
    "for iz in range(nz):\n",
    "    ax5.loglog(10.**logM, nu[iz]*f_nu[iz],     c=colors[iz])\n",
    "    ax5.loglog(10.**logM, nu[iz]*f_high_nu[iz],c=colors[iz],ls=':')\n",
    "    ax5.axvline(M_of_nu1[iz], c=colors[iz],ls=':',lw=1.)\n",
    "ax5.loglog(np.nan,np.nan, 'k:', label = 'approximation for $\\\\nu \\gg 1$')\n",
    "ax5.axvline(np.nan, c='k',ls=':',lw=0.5,label='$M(\\\\nu=1)$')\n",
    "ax5.set_xlabel('$M \\ [M_\\odot/h]$')\n",
    "ax5.set_ylabel('$\\\\nu f(\\\\nu)$')\n",
    "ax5.set_xlim(3e9,3e16)\n",
    "ax5.set_ylim(1e-4,1e1)\n",
    "ax5.legend()\n",
    "\n",
    "# Void size function\n",
    "for iz in range(nz):\n",
    "    ax6.loglog(R_Lag[iz], VSF[iz],c=colors[iz])\n",
    "ax6.set_xticks([5,10,20,50])\n",
    "ax6.set_xlabel('$R_\\mathrm{Lag} \\ [\\\\mathrm{Mpc}/h]$')\n",
    "ax6.set_ylabel('$\\\\frac{dn}{dR} \\ [(h/\\\\mathrm{Mpc})^4]$')\n",
    "ax6.set_xlim(5,40)\n",
    "ax6.set_ylim(1e-11,1e-3)\n",
    "\n",
    "\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "mounted-technique",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
